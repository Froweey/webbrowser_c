﻿using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBrowserTest
{
    /// <summary>
    /// Mechanize inspired stateful programmatic web browsing library for C#.
    /// </summary>
    public class Browser
    {
        public string website;
        public System.Net.HttpStatusCode status;
        public string reason;
        public HttpClient client = new HttpClient();
        HttpResponseMessage response;
        public string response_html;
        public string error;
        public bool success;
        /// <summary>
        /// Initializes a Browser() object and sets it default headers.
        /// </summary>
        public Browser()
        {
            SetHeaders();
        }
        /// <summary>
        /// Set the HttpClient(s) base address and update the Browser() website attribute.
        /// </summary>
        /// <param name="url">The base address to set</param>
        public void Visit(string url)
        {
            website = url;
            CheckUrl();
            client.BaseAddress = new Uri(website);
        }
        /// <summary>
        /// Open a webpage using our HttpClient and store its response and HTML (template) code.
        /// </summary>
        /// <param name="url">The exact webpage to open</param>
        /// <returns></returns>
        public async Task OpenAsync(string url)
        {
            try
            {
                website = url;
                CheckUrl();
                response = await client.GetAsync(website);
                response.EnsureSuccessStatusCode();
                status = response.StatusCode;
                reason = response.ReasonPhrase;
                response_html = await response.Content.ReadAsStringAsync();
                success = true;
                //Console.WriteLine(response_html);
            }
            catch (HttpRequestException hre)
            {
                success = false;
                error = hre.ToString();
            }
            catch (Exception ex)
            {
                success = false;
                error = ex.ToString();
            }
        }
        /// <summary>
        /// Set the headers for our HttpClient.
        /// </summary>
        /// <param name="user_agent">(Optional) string that defines our user-agent, Mozilla/5.0 by default.</param>
        public void SetHeaders(string user_agent = null)
        {
            if (user_agent != null)
            {
                client.DefaultRequestHeaders.Add("user-agent", user_agent);
            }
            else
            {
                client.DefaultRequestHeaders.Add("user-agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");
            }
        }
        /// <summary>
        /// Checks the website string and adds the http:// substring if its not already included.
        /// </summary>
        private void CheckUrl()
        {
            if (website.Substring(0, 7) != "http://")
            {
                website = "http://" + website;
            }
        }
        /// <summary>
        /// Checks the site string and adds the http:// substring if its not already included. Returns the editted (refactored) URL string.
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        public string RefactorUrl(string site)
        {
            if (site.Length > 0)
            {
                if (site.Substring(0, 7) != "http://")
                {
                    site = "http://" + site;
                }
            }
            return site;
        }
        /// <summary>
        /// Return a HttpResponseHeaders object containing the last requests HTTP response headers.
        /// </summary>
        /// <returns></returns>
        /*public Dictionary<KeyValuePair<string, IEnumerable<string>>, KeyValuePair<string, IEnumerable<string>>> Info()
        {
            try
            {
                var dict = response.Headers.ToDictionary(v => v, v => v);
                return dict;
            }
            catch (NullReferenceException nre)
            {
                error = nre.ToString();
                var dict = new Dictionary<KeyValuePair<string, IEnumerable<string>>, KeyValuePair<string, IEnumerable<string>>>();
                return dict;
            }
        }
        */
        public System.Net.Http.Headers.HttpResponseHeaders Info()
        {
            return response.Headers;
        }
        /// <summary>
        /// Send a POST request to the specified page (or resource) with the provided form_data.
        /// </summary>
        /// 
        /// 
        /// 
        /// <param name="page">String specifying the page to submit the POST data to</param>
        /// <param name="form_data">List with key,value pairs that contains our POST data</param>
        /// <returns></returns>
        public async Task SubmitPostAsync(string page, List<KeyValuePair<string, string>> form_data)
        {
            try
            {
                //var page_uri = new Uri(RefactorUrl(page));
                var content = new FormUrlEncodedContent(form_data);
                response = await client.PostAsync(page, content);
                response.EnsureSuccessStatusCode();
                status = response.StatusCode;
                reason = response.ReasonPhrase;
                var resp_html = await response.Content.ReadAsStringAsync();
                response_html = resp_html;
                success = true;
            }
            catch (HttpRequestException hre)
            {
                success = false;
                error = hre.ToString();
            }
            catch (Exception ex)
            {
                success = false;
                error = ex.ToString();
            }
        }
    }
    /// <summary>
    /// Class for testing the functionality in our Browser() class.
    /// </summary>
    class Test
    {
        public Test()
        {
            Start();
        }
        public delegate Task func_pointer(string url); // A.K.A function pointer
        private void Start()
        {
            Browser br = new Browser();
            Task returnedTask = null;
            Console.Write("Enter 1 for GET or 2 for POST test: ");
            var choice = Console.ReadLine();
            Console.Write("Enter a website to visit: ");
            string site = Console.ReadLine();
            if (site.Length > 0)
            {
                string dots = ".";
                Console.WriteLine("Navigating to website {0}", site);
                if (choice == "1")
                {
                    returnedTask = br.OpenAsync(site);
                }
                else if (choice == "2")
                {
                    br.Visit(site);
                    var google_search_form_data = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("q", "50 cent+zippyshare")
                    };
                    Console.Write("What page should we submit the post data to: ");
                    var post_page = Console.ReadLine();
                    returnedTask = br.SubmitPostAsync(post_page, google_search_form_data);
                }
                while (!returnedTask.IsCompleted)
                {
                    Console.Write("\rWorking{0}", dots);
                    if (dots.Length < 6)
                    {
                        dots = dots + ".";
                    }
                    else
                    {
                        dots = ".";
                    }
                }
                Console.Write("\rCompleted        ");
                Console.WriteLine("\nSuccessful: {0}", br.success.ToString());
                if (!br.success)
                {
                    Console.WriteLine("[An error occured!]");
                    Console.WriteLine(br.error.ToString());
                }
                Console.WriteLine(br.response_html);
                Console.WriteLine(br.Info().ToString());
            }
            Console.WriteLine("\nPress any key to exit!");
            Console.ReadKey();
        }
    }
    /// <summary>
    /// Program entry point.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Test t = new Test();
        }
    }
}
